import {FeedItemWrapperBase} from "./item/base-item-wrapper";
import {FeedItemBase} from "./item/base-item-data";

export class FeedResponse {
  public data: FeedItemWrapperBase<FeedItemBase>[];
  public nextPage?: number;
}