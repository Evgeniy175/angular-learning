import {Component} from '@angular/core';
import {state, style, transition, animate, trigger} from "@angular/animations";

@Component({
  selector: 'sandbox-bindings',
  templateUrl: 'animations.component.html',
  styleUrls: ['animations.component.scss'],
  animations: [
    trigger('isEven', [
      state('true', style({
        backgroundColor: '#eee',
        transform: 'scale(1)'
      })),
      state('false', style({
        backgroundColor: '#cfd8dc',
        transform: 'scale(1.5)'
      })),
      transition('0 => 1', animate('100ms ease-in')),
      transition('1 => 0', animate('100ms ease-out'))
    ])
  ],
})
export class SandboxAnimationsComponent {
  clickCounter = 0;

  onClick() {
    this.clickCounter++;
  }
}
