import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

import {HttpService} from "../../core/services/http/http.service";

import {InputFieldData} from "../../core/models/forms/input-field/field";
import {SignUpData} from "../../core/models/auth/sign-up/sign-up";

@Component({
  selector: 'sign-up',
  templateUrl: 'sign-up.component.html',
  styleUrls: ['sign-up.component.scss'],
})
export class SignUpComponent {
  private keys = {
    name: 'name',
    email: 'email',
    userName: 'userName',
    password: 'password',
  };

  private fieldsData = {
    [this.keys.name]: {
      placeholder: 'Full name',
      type: 'text',
      error: {
        text: 'Please enter your name',
        className: 'error',
      },
    },
    [this.keys.email]: {
      placeholder: 'Email',
      type: 'email',
      error: {
        text: 'Please enter valid email',
        className: 'error',
      },
    },
    [this.keys.userName]: {
      placeholder: 'Username',
      type: 'text',
      error: {
        text: 'Please enter user name',
        className: 'error',
      },
    },
    [this.keys.password]: {
      placeholder: 'Password',
      type: 'password',
      error: {
        text: 'Please enter valid password',
        className: 'error',
      },
    },
  };

  private signUpForm : FormGroup = new FormGroup({
    [this.keys.name]: new FormControl("", [Validators.required]),
    [this.keys.email]: new FormControl("", [Validators.required, Validators.email]),
    [this.keys.userName]: new FormControl("", [Validators.required]),
    [this.keys.password]: new FormControl("", [Validators.required]),
  });

  public fields: InputFieldData[] = [
    this.getFieldData(this.keys.name),
    this.getFieldData(this.keys.email),
    this.getFieldData(this.keys.userName),
    this.getFieldData(this.keys.password),
  ];

  private getFieldData(key: string): InputFieldData {
    const fieldData = this.fieldsData[key];
    return {
      key,
      input: {
        name: key,
        type: fieldData.type,
        placeholder: fieldData.placeholder,
        formControlName: key,
      },
      error: {
        text: fieldData.error.text,
        className: fieldData.error.className,
      },
    };
  }

  public constructor(private httpService: HttpService) {}

  public onSubmit() {
    if (!this.isValid()) return;
    this.httpService.auth.signUp(this.getRequestData());
  }

  private isValid(): boolean {
    let isValid = true;
    Object.keys(this.keys).forEach(key => {
      if (!this.validateControl(<FormControl>this.signUpForm.controls[this.keys[key]])) isValid = false;
    });
    return isValid;
  }

  private validateControl(control: FormControl): boolean {
    if (control.valid) return true;
    control.markAsTouched();
    return false;
  }

  private getRequestData() {
    let req: SignUpData = new SignUpData();
    Object.keys(this.keys).forEach(key => {
      req[this.keys[key].toLowerCase()] = this.signUpForm.controls[this.keys[key]].value;
    });
    return req;
  }
}
