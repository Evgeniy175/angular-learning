import {Component} from '@angular/core';
import {NgForm, FormControl} from "@angular/forms";

import {LoginData} from '../../../../core/models/auth/login/login';
import {HttpService} from "../../../../core/services/http/http.service";

@Component({
  selector: 'login-form',
  templateUrl: 'login-form.component.html',
  styleUrls: ['login-form.component.scss'],
})
export class LoginFormComponent {
  data: LoginData = new LoginData("", "");

  public constructor(private httpService: HttpService) {}

  public onSubmit(form: NgForm) {
    if (!this.isValid(form)) return;
    this.httpService.auth.login(this.data);
  }

  private isValid(form: NgForm): boolean {
    let isValid = true;

    if (!this.validateControl(<FormControl>form.controls.email)) isValid = false;
    if (!this.validateControl(<FormControl>form.controls.password)) isValid = false;

    return isValid;
  }

  private validateControl(control: FormControl): boolean {
    if (control.valid) return true;
    control.markAsTouched();
    return false;
  }
}
