import {Injectable} from "@angular/core";

import {User} from "../../models/user/user";

import {LoginResponse} from "../../models/auth/login/login-response";

@Injectable()
export class AuthService {
  private readonly userIdKey: string = 'dickit-user-id';
  private readonly tokenKey: string = 'dickit-token';

  private userData: User;
  private userIdData: string = localStorage.getItem(this.userIdKey);
  private userToken: string = localStorage.getItem(this.tokenKey);

  public get userId() {
    return this.userIdData;
  }
  public set userId(value: string) {
    this.userIdData = value;
    this.setLocalStorage(this.userIdKey, value);
  }

  public get token() {
    return this.userToken;
  }
  public set token(value: string) {
    this.userToken = value;
    this.setLocalStorage(this.tokenKey, value);
  }

  public get isSigned() {
    return !!this.token;
  }

  public get user() : User {
    return this.userData;
  }
  public set user(user: User) {
    this.userData = user;
    this.userId = user ? user.id : null;
  }

  public init(data: LoginResponse) {
    this.token = data.token;
    this.user = data.user;
  }

  public clear() : void {
    this.user = null;
    this.clearToken();
  }

  private clearToken() : void {
    this.token = null;
    localStorage.removeItem(this.tokenKey);
  }

  private setLocalStorage(key: string, value?: string): void {
    if (value) localStorage.setItem(key, value);
    else localStorage.removeItem(key);
  }
}
