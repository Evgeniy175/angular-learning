import {User} from "../../user/user";

export class FeedItemBase {
  public id: string;
  public created_at?: string;
  public message?: string;
  public targetUser?: User;
  public image?: string;
  public imageUrl?: string;
  public title?: string;
  public user?: User;
}
