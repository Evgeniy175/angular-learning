import {Component, OnInit} from '@angular/core';

import {AuthService} from "./core/services/auth/auth.service";
import {HttpService} from "./core/services/http/http.service";

import {UserResponse} from "./core/models/user/user-response";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'DickIt Calendar';

  public constructor(private authService: AuthService,
                     private httpService: HttpService) {}

  ngOnInit() {
    const { userId } = this.authService;
    if (!userId) return;
    return this.httpService.auth.getUser(userId)
    .subscribe((res: UserResponse) => {
      this.authService.user = res.data;
    });
  }
}
