import {Component, Input} from '@angular/core';

import {ImageService} from "../../../../core/services/image/image.service";

import {FeedCommentItemWrapper} from "../../../../core/models/feed/item/comment/wrapper";

@Component({
  selector: 'feed-comment-item',
  templateUrl: 'comment.component.html',
  styleUrls: ['comment.component.scss']
})
export class FeedCommentComponent {
  @Input() item: FeedCommentItemWrapper;

  private readonly modelName = 'comments';

  public get username() {
    return this.data.user.username;
  }

  public get imageUrl() {
    const { imageUrl } = this.data;
    if (!imageUrl) return;
    return this.imageService.formatUrl(this.modelName, imageUrl);
  }

  public get createdAt() {
    return this.data.created_at;
  }

  public get text() {
    return this.data.message;
  }

  private get data() {
    return this.item.data;
  }

  public constructor(private imageService: ImageService) {}
}
