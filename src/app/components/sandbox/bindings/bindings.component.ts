import {Component} from '@angular/core';

@Component({
  selector: 'sandbox-bindings',
  templateUrl: 'bindings.component.html',
  styleUrls: ['bindings.component.scss']
})
export class SandboxBindingsComponent {
  oneWay: string = 'first';
  twoWay: string = 'second';
}
