import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {BrowserModule} from "@angular/platform-browser";

import {NavBarComponent} from './components/navbar/navbar.component';

import {HttpService} from './services/http/http.service';
import {AuthService} from './services/auth/auth.service';
import {ImageService} from './services/image/image.service';

import {OnlyLoggedGuard} from './guards/only-logged.guard';
import {OnlyNotLoggedGuard} from './guards/only-not-logged.guard';

import {TimestampPipe} from './pipes/timestamp/timestamp.pipe'

@NgModule({
  declarations: [
    // components
    NavBarComponent,

    // pipes
    TimestampPipe,
  ],
  imports: [
    BrowserModule,
    RouterModule,
  ],
  exports: [
    NavBarComponent,
    TimestampPipe,
  ],
  providers: [
    HttpService,
    AuthService,
    ImageService,
    OnlyLoggedGuard,
    OnlyNotLoggedGuard,
  ],
})

export class CoreModule { }
