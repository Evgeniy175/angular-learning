import {FeedItemWrapperBase} from "../base-item-wrapper";

import {FeedCreationItem} from "./data";

export class FeedCreationItemWrapper extends FeedItemWrapperBase<FeedCreationItem> {}