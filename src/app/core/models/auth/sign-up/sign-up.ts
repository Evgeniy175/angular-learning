export class SignUpData {
  public name: string;
  public email: string;
  public password: string;
  public username: string;
}
