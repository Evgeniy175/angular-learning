import {Component} from '@angular/core';

import {ILink} from './models/i-link';

import {AuthService} from "../../services/auth/auth.service";

@Component({
  selector: 'dickit-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.scss']
})
export class NavBarComponent {
  rightUnsignedItems: ILink[] = [
    { text: 'Log In', href: '/login' },
    { text: 'Sign Up', href: '/sign-up' },
  ];
  rightSignedItems: ILink[] = [
    { text: 'Log Out', href: '/logout' },
  ];
  public constructor(private authService: AuthService) {}
}
