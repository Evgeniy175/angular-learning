import {Component, Input} from '@angular/core';

import {FeedCreationItemWrapper} from "../../../../core/models/feed/item/creation/wrapper";

import {ImageService} from "../../../../core/services/image/image.service";

@Component({
  selector: 'feed-event-item',
  templateUrl: 'event.component.html',
  styleUrls: ['event.component.scss']
})
export class FeedEventComponent {
  @Input() item: FeedCreationItemWrapper;

  private readonly modelName = 'events';

  public get username() {
    return this.data.user.username;
  }

  public get imageUrl() {
    const { image } = this.data;
    if (!image) return;
    return this.imageService.formatUrl(this.modelName, image);
  }

  public get createdAt() {
    return this.data.created_at;
  }

  public get title() {
    return this.data.title;
  }

  private get data() {
    return this.item.data;
  }

  public constructor(private imageService: ImageService) {}
}
