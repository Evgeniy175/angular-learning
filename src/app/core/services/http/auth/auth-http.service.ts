import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";

import {BaseHttpService} from '../base/base.service';
import {AuthService} from '../../auth/auth.service';

import {LoginData} from '../../../models/auth/login/login';
import {LoginResponse} from "../../../models/auth/login/login-response";

import {SignUpData} from "../../../models/auth/sign-up/sign-up";
import {SignUpResponse} from "../../../models/auth/sign-up/sign-up-response";

import {RequestConfig} from '../base/models/request-config';

import {AppConfig} from "../../../../config/app.config";
import {RequestTypes} from "../base/models/request-types";

export class AuthHttpService extends BaseHttpService {
  private readonly userModelBase = `/users`;
  private readonly requests = {
    signUp: (data?: any) => new RequestConfig(RequestTypes.Post, `${this.userModelBase}`, data),
    login: (data?: any) => new RequestConfig(RequestTypes.Post, `${this.userModelBase}/login`, data),
    logout: (data?: any) => new RequestConfig(RequestTypes.Post, `${this.userModelBase}/logout`, data),
    profileById: (id: string, data?: any) => new RequestConfig(RequestTypes.Get, `${this.userModelBase}/${id}/decorated`, data),
  };

  public constructor(http: HttpClient,
                     authService: AuthService,
                     private router: Router) {
    super(http, authService);
  }

  signUp(data: SignUpData) {
    const request: RequestConfig = this.requests.signUp(data);
    return super.doRequest(request).subscribe((data: SignUpResponse) => {
      this.authService.init(data);
      this.router.navigate([AppConfig.routes.feed]);
    });
  }

  login(data: LoginData) {
    const request: RequestConfig = this.requests.login(data);
    return super.doRequest(request).subscribe((data: LoginResponse) => {
      this.authService.init(data);
      this.router.navigate([AppConfig.routes.feed]);
    });
  }

  logout() {
    const request: RequestConfig = this.requests.logout();
    this.authService.clear();
    this.router.navigate([AppConfig.routes.home]);
    return super.doRequest(request);
  }

  getUser(id: string) : Observable<Object> {
    const request: RequestConfig = this.requests.profileById(id);
    return super.doRequest(request);
  }
}
