import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {Observable} from "rxjs/Rx";
import {Injectable} from "@angular/core";

import {AuthService} from "../services/auth/auth.service";

import {AppConfig} from '../../config/app.config';

@Injectable()
export class OnlyNotLoggedGuard implements CanActivate {
  public constructor(private authService: AuthService, private router: Router) {};

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> | boolean {
    const isSigned = this.authService.isSigned;
    if (isSigned) this.router.navigate([AppConfig.routes.feed]);
    return !isSigned;
  }
}