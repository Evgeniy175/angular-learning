export const AppConfig = {
  routes: {
    home: '',
    auth: {
      logIn: 'login',
      signUp: 'sign-up',
      logOut: 'logout',
    },
    feed: 'feed',
    sandbox: 'sandbox',
  },
  request: {
    endpoints: {
      base: 'http://api.dockit.me',
    },
  }
};
