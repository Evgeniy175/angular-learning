export class ErrorData {
  public text?: string;
  public className?: string;
  public id?: string;
}
