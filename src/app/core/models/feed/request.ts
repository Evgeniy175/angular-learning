import {RequestParams} from "../base/request/request-params";

export class FeedRequest {
  public params: RequestParams;

  public constructor(limit?: number, offset?: number, page?: number) {
    this.params = new RequestParams(limit, offset, page);
  }
}