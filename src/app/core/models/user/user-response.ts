import {User} from "./user";

export class UserResponse {
  public data: User;
}
