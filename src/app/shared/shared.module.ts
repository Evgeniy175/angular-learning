import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from '@angular/forms';

import {ButtonComponent} from "./components/button/button.component";
import {ImageComponent} from "./components/image/image.component";
import {LoginFormComponent} from './components/auth/login-form/login-form.component';

@NgModule({
  declarations: [
    ButtonComponent,
    ImageComponent,
    LoginFormComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
  ],
  exports: [
    ButtonComponent,
    ImageComponent,
    LoginFormComponent,
  ],
})

export class SharedModule {}
