import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

import {AuthHttpService} from './auth/auth-http.service';
import {FeedHttpService} from './feed/feed-http.service';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class HttpService {
  public auth: AuthHttpService;
  public feed: FeedHttpService;

  public constructor(private http: HttpClient,
                     private authService: AuthService,
                     private router: Router) {
    this.auth = new AuthHttpService(http, authService, router);
    this.feed = new FeedHttpService(http, authService);
  }
}
