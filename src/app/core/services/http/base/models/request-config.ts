import {HttpHeaders, HttpParams} from '@angular/common/http';

import {RequestTypes} from "./request-types";

export class RequestConfig {
  public body: string;
  public params: HttpParams;
  public headers: HttpHeaders;

  public constructor(public type: RequestTypes,
                     public uri: string,
                     data: any,
                     headers?: HttpHeaders) {
    this.handleData(data);
    this.headers = headers || new HttpHeaders();
  }

  private handleData(data: Object) {
    switch(this.type) {
      case RequestTypes.Get:
      case RequestTypes.Delete:
        if (!data) break;
        this.params = new HttpParams();
        Object.keys(data).forEach(key => {
          if (!data[key]) return;
          this.params = this.params.set(key, data[key]);
        });
        break;
      case RequestTypes.Post:
      case RequestTypes.Put:
        this.body = JSON.stringify(data);
        break;
    }
  }
}
