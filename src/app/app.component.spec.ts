import {TestBed, async} from '@angular/core/testing';

import {AppModule} from './app.module';
import {AppComponent} from './app.component';

describe('AppComponent', () => {
  const brandSrc = '/assets/images/brand_beta.png';
  const title = 'DickIt Calendar';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule,
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title '${title}'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('DickIt Calendar');
  }));
  it('should contain logo image', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const imagesElems = compiled.getElementsByTagName('img');
    const images = Array.from(imagesElems);
    const isAnyExists = images.some((image: HTMLImageElement) => image.src.endsWith(brandSrc));
    expect(isAnyExists).toBeTruthy();
  }));
  it('should contain log in link', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const linksElems = compiled.getElementsByTagName('a');
    const links = Array.from(linksElems);
    const isAnyExists = links.some((link: HTMLLinkElement) => link.textContent === 'Log In');
    expect(isAnyExists).toBeTruthy();
  }));
  it('should contain sign up link', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const linksElems = compiled.getElementsByTagName('a');
    const links = Array.from(linksElems);
    const isAnyExists = links.some((link: HTMLLinkElement) => link.textContent === 'Sign Up');
    expect(isAnyExists).toBeTruthy();
  }));
});
