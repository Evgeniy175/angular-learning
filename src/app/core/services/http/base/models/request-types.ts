export enum RequestTypes {
  Get,
  Post,
  Put,
  Delete,
}
