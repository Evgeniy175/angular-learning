import {Component, Input} from '@angular/core';

@Component({
  selector: 'dickit-image',
  templateUrl: 'image.component.html',
  styleUrls: ['image.component.scss'],
})
export class ImageComponent {
  @Input() src?: string;

  public constructor() {}
}
