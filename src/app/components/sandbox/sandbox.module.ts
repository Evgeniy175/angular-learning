import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {SandboxComponent} from "./sandbox.component";

import {SandboxBindingsComponent} from './bindings/bindings.component';
import {SandboxAnimationsComponent} from './animations/animations.component';
import {SandboxMenuComponent} from './menu/menu.component';

@NgModule({
  declarations: [
    SandboxComponent,
    SandboxBindingsComponent,
    SandboxAnimationsComponent,
    SandboxMenuComponent,
  ],
  imports: [
    RouterModule,
    FormsModule,
    BrowserAnimationsModule,
    /*BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,*/
  ],
})
export class SandboxModule { }
