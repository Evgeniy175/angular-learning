import {FeedItemBase} from "./base-item-data";

export class FeedItemWrapperBase<T extends FeedItemBase> {
  public data: T;
  public type: string;
  public action: string;
  public timestamp: string;
}
