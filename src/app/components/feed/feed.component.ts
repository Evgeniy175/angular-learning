import {Component, OnInit, HostListener, ElementRef} from '@angular/core';

import {HttpService} from "../../core/services/http/http.service";

import {FeedRequest} from "../../core/models/feed/request";
import {FeedResponse} from "../../core/models/feed/response";
import {FeedItemWrapperBase} from "../../core/models/feed/item/base-item-wrapper";
import {FeedItemBase} from "../../core/models/feed/item/base-item-data";

@Component({
  selector: 'feed',
  templateUrl: 'feed.component.html',
  styleUrls: ['feed.component.scss'],
})
export class FeedComponent implements OnInit {
  public items: FeedItemWrapperBase<FeedItemBase>[];
  public isInitiallyLoaded: boolean = false;

  private readonly limit: number = 20;
  private nextPage?: number;
  private offset: number;
  private requestData: FeedRequest;

  public constructor(
    private elem: ElementRef,
    private httpService: HttpService) {}

  ngOnInit() {
    this.requestData = new FeedRequest(this.limit);
    this.offset = this.limit;
    this.httpService.feed.fetchFeed(this.requestData).subscribe((res: FeedResponse) => {
      this.items = res.data;
      this.nextPage = res.nextPage;
      this.isInitiallyLoaded = true;
    });
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(e) {
    if (this.isNeedToLoad()) this.loadMore();
  }

  private isNeedToLoad() : boolean {
    if (!this.nextPage) return false;

    const clientHeight: number = window.innerHeight;
    const offset: number = window.pageYOffset;
    const height: number = this.elem.nativeElement.getElementsByClassName('feed-wrapper')[0].scrollHeight;

    // when 50% or more scrolled
    return (clientHeight + offset) / height >= 0.5;
  }

  private loadMore() : void {
    this.requestData = new FeedRequest(this.limit, this.offset);
    this.offset += this.limit;
    this.httpService.feed.fetchFeed(this.requestData).subscribe((res: FeedResponse) => {
      this.items = this.items.concat(res.data);
      this.nextPage = res.nextPage;
    });
  }
}
