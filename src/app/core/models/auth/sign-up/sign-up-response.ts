import {User} from "../../user/user";

export class SignUpResponse {
  public token: string;
  public user: User;
}
