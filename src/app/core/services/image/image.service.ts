import {Injectable} from "@angular/core";

import {ImageResolution} from "../../models/image/resolution/resolution";

@Injectable()
export class ImageService {
  private readonly defaultResolution = new ImageResolution(640, 640);

  public formatUrl(model: string, imageKey: string, resolution?: ImageResolution): string {
    resolution = resolution || this.defaultResolution;
    const { w, h } = resolution;
    return `https://d37ifxawxxt4jh.cloudfront.net/images/${model}/${w}x${h}_${imageKey}`;
  }
}
