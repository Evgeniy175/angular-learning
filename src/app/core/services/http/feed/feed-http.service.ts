import {HttpClient} from "@angular/common/http";

import {BaseHttpService} from '../base/base.service';
import {AuthService} from '../../auth/auth.service';

import {FeedRequest} from "../../../models/feed/request";

import {RequestConfig} from '../base/models/request-config';
import {RequestTypes} from "../base/models/request-types";

export class FeedHttpService extends BaseHttpService {
  private readonly userModelBase = `/feed`;
  private readonly requests = {
    feed: data => new RequestConfig(RequestTypes.Get, `${this.userModelBase}`, data),
  };

  public constructor(http: HttpClient, authService: AuthService) {
    super(http, authService);
  }

  fetchFeed(data: FeedRequest) {
    const request: RequestConfig = this.requests.feed(data.params);
    return super.doRequest(request);
  }
}
