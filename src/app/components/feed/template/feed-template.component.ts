import {Component, ContentChild, TemplateRef, Input} from '@angular/core';

@Component({
  selector: 'feed-template',
  templateUrl: 'feed-template.component.html',
  styleUrls: ['feed-template.component.scss'],
})
export class FeedTemplateComponent {
  @ContentChild('commentTemplate', {read: TemplateRef}) commentTemplate: TemplateRef<any>;
  @ContentChild('eventTemplate', {read: TemplateRef}) eventTemplate: TemplateRef<any>;
  @Input() items: Array<any> = [];
}