import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'timestamp'
})
export class TimestampPipe implements PipeTransform {
  private readonly formatter = 'll';

  transform(value: string, args?: any): string {
    return value ? moment(value).format(this.formatter) : null;
  }
}
