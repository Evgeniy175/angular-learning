export class User {
  public id: string;
  public username: string;
  public name: string;
  public email: string;
  public description: string;
}
