import {TestBed, ComponentFixture} from '@angular/core/testing';

import {AppModule} from '../../app.module';

import {SignUpComponent} from "./sign-up.component";

describe('Component: Sign Up', () => {
  let fixture: ComponentFixture<SignUpComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
    });
    fixture = TestBed.createComponent(SignUpComponent);
    fixture.detectChanges();
  });

  it(`has 'Full name' input field`, () => {
    expect(isInputWithPlaceHolderExists('Full name')).toBeTruthy();
  });

  it(`has 'Email' input field`, () => {
    expect(isInputWithPlaceHolderExists('Email')).toBeTruthy();
  });

  it(`has 'Username' input field`, () => {
    expect(isInputWithPlaceHolderExists('Username')).toBeTruthy();
  });

  it(`has 'Password' input field`, () => {
    expect(isInputWithPlaceHolderExists('Password')).toBeTruthy();
  });

  it(`validate that 'Email' input field has type 'email'`, () => {
    const emailField = fixture.debugElement.nativeElement.querySelector(`input[type=email]`);
    expect(emailField.placeholder === 'Email').toBeTruthy();
  });

  it(`has 'Sign Up' button`, () => {
    const submitButton: HTMLButtonElement = fixture.debugElement.nativeElement.querySelector(`button[type=submit]`);
    expect(submitButton).toBeTruthy();
    expect(submitButton.textContent === 'Sign Up').toBeTruthy();
  });

  it(`fill form`, () => {
    writeInto('name', `Evgeniy ${Math.random()}`);
    writeInto('email', `evgeniy${Math.random()}@gmail.com`);
    writeInto('userName', `EvgeNaN_${Math.random()}`);
    writeInto('password', `lolkek123`);

    expect(isInputWithPlaceHolderExists('Password')).toBeTruthy();
  });

  function writeInto(name: string, text: string) {
    const input = findInputByName(name);
    if (!input) return;
    input.value = text;
    input.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    return fixture.whenStable();
  }

  function isInputWithPlaceHolderExists(placeholder: string) {
    const compiled = fixture.debugElement.nativeElement.getElementsByTagName('input');
    const inputs = Array.from(compiled);
    return inputs.some((input: HTMLInputElement) => input.placeholder === placeholder);
  }

  function findInputByName(name: string) {
    return fixture.debugElement.nativeElement.querySelector(`input[name=${name}]`);
  }
});