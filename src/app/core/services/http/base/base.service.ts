import {HttpClient, HttpHeaders} from '@angular/common/http';

import {AuthService} from "../../auth/auth.service";

import {RequestConfig} from './models/request-config';

import {AppConfig} from '../../../../config/app.config';
import {RequestTypes} from "./models/request-types";

export class BaseHttpService {
  private readonly base: any = AppConfig.request.endpoints.base;
  private readonly contentType: string = 'application/json';

  public constructor(private http: HttpClient, protected authService: AuthService) {}

  public doRequest(config: RequestConfig) {
    this.attachHeaders(config);
    const uri: string = `${this.base}${config.uri}`;

    switch (config.type) {
      case RequestTypes.Get:
        return this.http.get(uri, config);
      case RequestTypes.Post:
        return this.http.post(uri, config.body, config);
      case RequestTypes.Put:
        return this.http.put(uri, config.body, config);
      case RequestTypes.Delete:
        return this.http.delete(uri, config);
    }
  }

  attachHeaders(config: RequestConfig): void {
    const token = this.authService.token;
    config.headers = config.headers || new HttpHeaders();
    if (token) config.headers = config.headers.set('Authorization', token);
    config.headers = config.headers.set('Content-Type', this.contentType);
  }
}
