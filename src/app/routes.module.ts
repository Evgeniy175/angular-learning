import {NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomeComponent} from './components/home/home.component';
import {SignUpComponent} from './components/sign-up/sign-up.component';
import {LogInComponent} from './components/login/login.component';
import {LogOutComponent} from './components/logout/logout.component';
import {FeedComponent} from './components/feed/feed.component';

import {SandboxComponent} from './components/sandbox/sandbox.component';
import {SandboxBindingsComponent} from './components/sandbox/bindings/bindings.component';
import {SandboxAnimationsComponent} from './components/sandbox/animations/animations.component';

import {OnlyLoggedGuard} from './core/guards/only-logged.guard';
import {OnlyNotLoggedGuard} from './core/guards/only-not-logged.guard';

import {AppConfig} from './config/app.config';

const sandboxRoutes: Routes = [
  { path: 'bindings', component: SandboxBindingsComponent },
  { path: 'animations', component: SandboxAnimationsComponent },
];

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [OnlyNotLoggedGuard] },
  { path: AppConfig.routes.auth.signUp, component: SignUpComponent, canActivate: [OnlyNotLoggedGuard] },
  { path: AppConfig.routes.auth.logIn, component: LogInComponent, canActivate: [OnlyNotLoggedGuard] },
  { path: AppConfig.routes.feed, component: FeedComponent, canActivate: [OnlyLoggedGuard] },
  { path: AppConfig.routes.auth.logOut, component: LogOutComponent, canActivate: [OnlyLoggedGuard] },
  { path: AppConfig.routes.sandbox, component: SandboxComponent, children: sandboxRoutes },
  { path: '**', redirectTo: AppConfig.routes.feed },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}
