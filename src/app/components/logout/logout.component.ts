import {Component, OnInit} from '@angular/core';

import {HttpService} from "../../core/services/http/http.service";

@Component({
  selector: 'logout',
  template: '',
})
export class LogOutComponent implements OnInit {
  public constructor(private httpService: HttpService) {}

  ngOnInit() {
    this.httpService.auth.logout();
  }
}
