export class InputData {
  public name: string;
  public type?: any;
  public className?: string;
  public id?: string;
  public placeholder?: string;
  public formControlName: string;
}
