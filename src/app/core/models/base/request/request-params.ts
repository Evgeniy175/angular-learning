export class RequestParams {
  public constructor(public limit?: number,
                     public offset?: number,
                     public page?: number) {}
}
