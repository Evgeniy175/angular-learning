import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF} from "@angular/common";

import {AppRoutingModule} from './routes.module';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';

import {AppComponent} from './app.component';

import {HomeComponent} from "./components/home/home.component";
import {SignUpComponent} from "./components/sign-up/sign-up.component";
import {LogInComponent} from "./components/login/login.component";
import {LogOutComponent} from "./components/logout/logout.component";

import {FeedComponent} from "./components/feed/feed.component";
import {FeedCommentComponent} from "./components/feed/items/comment/comment.component";
import {FeedEventComponent} from "./components/feed/items/event/event.component";
import {FeedTemplateComponent} from "./components/feed/template/feed-template.component";

import {SandboxModule} from "./components/sandbox/sandbox.module";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignUpComponent,
    LogInComponent,
    LogOutComponent,
    FeedComponent,
    FeedCommentComponent,
    FeedEventComponent,
    FeedTemplateComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    SandboxModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/'},
  ],
})
export class AppModule { }
