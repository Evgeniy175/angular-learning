import {ErrorData} from "../error/error";
import {InputData} from "../input/input";

export class InputFieldData {
  public key?: string;
  public input?: InputData;
  public error?: ErrorData;
}
