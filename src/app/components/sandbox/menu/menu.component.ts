import { Component } from '@angular/core';

@Component({
  selector: 'sandbox-menu',
  templateUrl: 'menu.component.html',
  styleUrls: ['menu.component.scss']
})
export class SandboxMenuComponent {}
