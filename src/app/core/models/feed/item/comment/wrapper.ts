import {FeedItemWrapperBase} from "../base-item-wrapper";

import {FeedCommentItem} from "./data";

export class FeedCommentItemWrapper extends FeedItemWrapperBase<FeedCommentItem> {}